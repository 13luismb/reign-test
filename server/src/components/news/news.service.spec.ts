import { HttpModule } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ApiService } from '../../services/api/api.service';
import { News } from './news.interfaces';
import { NewsService } from './news.service';
import { random, lorem, internet, date } from 'faker';
import { Model } from 'mongoose';

describe('NewsService', () => {
  let service: NewsService;
  let model: Model<News>;
  const NEWS_LENGTH = 10;
  const generateFakeNews = (): Partial<News> => {
    return {
      _id: random.uuid(),
      title: lorem.sentence(),
      author: internet.userName(),
      createdAt: date.recent(),
      url: internet.url(),
      deleted: false,
    };
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApiService,
        NewsService,
        {
          provide: getModelToken('News'),
          useValue: {
            new: jest.fn().mockResolvedValue(generateFakeNews()),
            constructor: jest.fn().mockResolvedValue(generateFakeNews()),
            find: jest.fn(),
            sort: jest.fn(),
            exec: jest.fn(),
            updateOne: jest.fn(),
            findByIdAndUpdate: jest.fn(),
          },
        },
      ],
      imports: [HttpModule],
    }).compile();

    service = module.get<NewsService>(NewsService);
    model = module.get<Model<News>>(getModelToken('News'));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('should return all news', () => {
    it('getNews', async () => {
      jest.spyOn(service, 'getNews').mockImplementation(async () => new Array(NEWS_LENGTH).fill({}).map((_) => generateFakeNews()));
      const news = await service.getNews();
      expect(news).toHaveLength(NEWS_LENGTH);
    });
  });

  describe('should delete a news post', () => {
    it('deleteNews', async () => {
      const UUID = random.uuid();
      jest.spyOn(model, 'updateOne').mockResolvedValue({ nModified: 0 }).mockResolvedValueOnce({ nModified: 1 });
      expect(await service.deleteNews(UUID)).toEqual({ message: 'News deleted succesfully' });
      expect(model.updateOne).toBeCalled();
    });
  });
});
