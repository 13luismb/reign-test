import * as mongoose from 'mongoose';

export const NewsSchema = new mongoose.Schema({
  _id: { type: String, required: true },
  title: { type: String, required: true },
  author: { type: String, required: true },
  url: { type: String, required: true },
  createdAt: { type: Date, required: true },
  deleted: { type: Boolean, default: false },
});
