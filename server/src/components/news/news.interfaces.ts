import * as mongoose from 'mongoose';

export interface News extends mongoose.Document {
  _id: string;
  title: string;
  author: string;
  url: string;
  createdAt: Date;
  deleted: boolean;
}
