import { Controller, Get, Param, Patch } from '@nestjs/common';
import { News } from './news.interfaces';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
  constructor(private readonly news: NewsService) {}

  @Get()
  getNews(): Promise<Partial<News>[]> {
    return this.news.getNews();
  }

  @Patch(':id')
  deleteNews(@Param() params: { id: string }): Promise<{ message: string }> {
    return this.news.deleteNews(params.id);
  }
}
