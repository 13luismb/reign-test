import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiService } from '../../services/api/api.service';
import { NewsController } from './news.controller';
import { NewsSchema } from './news.model';
import { NewsService } from './news.service';

@Module({
  imports: [HttpModule, MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }])],
  controllers: [NewsController],
  providers: [NewsService, ApiService],
  exports: [NewsService],
})
export class NewsModule {}
