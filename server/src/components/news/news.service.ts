import { BadRequestException, Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ApiService } from '../../services/api/api.service';
import { News } from './news.interfaces';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class NewsService implements OnApplicationBootstrap {
  constructor(@InjectModel('News') private readonly newsModel: Model<News>, private api: ApiService) {}
  private readonly logger = new Logger(NewsService.name);

  onApplicationBootstrap() {
    this.logger.debug('Fetching news from api...');
    try {
      this.fetchNews();
    } catch (e) {
      throw e;
    }
  }

  private fetchNews(): void {
    try {
      this.api.fetchApi().subscribe(async (response) => {
        const { hits } = response.data;
        const curatedNews = hits.filter((el) => el.story_title || el.title).filter((el) => el.story_url || el.url);

        const result = curatedNews?.map((news) => {
          const payload: Partial<News> = {
            _id: news.objectID,
            title: news.story_title || news.title,
            author: news.author,
            url: news.story_url || news.url,
            createdAt: news.created_at,
          };
          const createdNews = this.insertNews(payload);
          return createdNews;
        });
        return await Promise.all(result);
      });
    } catch (error) {
      throw error;
    }
  }

  private async insertNews({ _id, createdAt, title, author, url }: Partial<News>) {
    try {
      const result = await this.newsModel.findByIdAndUpdate(
        _id,
        { _id, createdAt, title, author, url },
        { upsert: true, setDefaultsOnInsert: true, useFindAndModify: false },
      );
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getNews(): Promise<Partial<News>[]> {
    try {
      const news = await this.newsModel.find({ deleted: false }).sort({ createdAt: 'desc' }).exec();
      const result: Partial<News>[] = news.map((el) => ({
        _id: el._id,
        createdAt: el.createdAt,
        title: el.title,
        author: el.author,
        url: el.url,
      }));
      return result;
    } catch (error) {
      throw error;
    }
  }

  async deleteNews(id: string): Promise<{ message: string }> {
    try {
      const deletedPost = await this.newsModel.updateOne({ _id: id }, { $set: { deleted: true } });
      if (deletedPost.nModified === 1) {
        return { message: 'News deleted succesfully' };
      } else {
        throw new BadRequestException({ message: 'Could not delete selected news' });
      }
    } catch (error) {
      throw error;
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    this.logger.debug('Called every hour');
    this.fetchNews();
  }
}
