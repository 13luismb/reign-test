import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let controller: NewsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [
        {
          provide: NewsService,
          useValue: {
            getNews: jest.fn(),
            deleteNews: jest.fn().mockImplementation(() => Promise.resolve({ message: 'Could not delete selected news' })),
          },
        },
      ],
    }).compile();
    controller = app.get<NewsController>(NewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('Should return message: "Could not delete selected news"', () => {
    it('deleteNews', async () => {
      expect(await controller.deleteNews({ id: Math.random().toString() })).toEqual({ message: 'Could not delete selected news' });
    });
  });
});
