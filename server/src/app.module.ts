import { HttpModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsModule } from './components/news/news.module';

@Module({
  imports: [
    HttpModule,
    NewsModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(process.env.DB_CONNECTION || 'mongodb://172.17.0.3:27017/reign-test'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
