import { HttpService, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { ApiData } from './api.interfaces';

@Injectable()
export class ApiService {
  constructor(private http: HttpService) {}

  fetchApi(): Observable<AxiosResponse<ApiData>> {
    return this.http.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs');
  }
}
