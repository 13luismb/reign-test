export interface ApiNews {
  objectID: string;
  created_at: Date;
  title: string;
  url: string;
  author: string;
  story_title: string;
  story_url: string;
  story_id: number;
}

export interface ApiData {
  hits: ApiNews[];
}
