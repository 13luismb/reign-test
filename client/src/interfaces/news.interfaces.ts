export interface NewsProps {
   _id: string;
   title: string;
   author: string;
   url: string;
   createdAt: Date;
   deleted: boolean;
   handleDelete: (_id: string) => Promise<boolean>;
}
