import React from 'react';
import moment from 'moment';
import { NewsProps } from '../../interfaces/news.interfaces';
import Trash from '../Trash';
import './News.css';

const News: React.FC<NewsProps> = ({ _id, title, url, author, createdAt, handleDelete }) => {
   return (
      <li onClick={() => window.open(url, '_blank')}>
         <div style={{ display: 'flex', marginTop: '5px', marginLeft: '1vw' }}>
            <h3 className={'Title'} style={{ color: '#333', fontSize: '13pt', marginRight: '7px' }}>
               {title}.
            </h3>
            <p className={'Author'} style={{ color: '#999' }}>
               - {author} -
            </p>
         </div>
         <div style={{ display: 'flex', marginRight: '2vw' }}>
            <h3 className={'Date'} style={{ marginRight: '35px', color: '#333' }}>
               {moment(createdAt).calendar({
                  sameDay: 'hh:mm a',
                  lastDay: '[Yesterday]',
                  lastWeek: 'MMM DD',
                  sameElse: 'MMM DD',
               })}
            </h3>
            <Trash
               onClick={(e) => {
                  e.stopPropagation();
                  handleDelete(_id);
               }}
            />
         </div>
      </li>
   );
};

export default News;
