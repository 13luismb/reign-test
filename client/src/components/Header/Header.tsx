import React from 'react';

const Header: React.FC = () => {
   return (
      <header className='App-header'>
         <h1 style={{ fontSize: '4em', margin: '40px 10px 1px 20px' }}>
            <code>HN</code> Feed
         </h1>
         <h3 style={{ margin: '0 10px 40px 25px' }}>We {'<3'} Hacker News!</h3>
      </header>
   );
};

export default Header;
