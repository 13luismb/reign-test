import React, { useEffect, useState } from 'react';
import { getNews, patchNews } from '../../services/news';
import News from '../News/News';
import { NewsProps } from './../../interfaces/news.interfaces';
import './NewsContainer.css';

const NewsContainer: React.FC = () => {
   const [news, setNews] = useState<NewsProps[]>([]);
   const [loading, setLoading] = useState(true);

   const fetchNews = async (): Promise<void> => {
      try {
         const news: NewsProps[] = await getNews();
         setNews([...news]);
         setLoading(false);
         console.log(news);
      } catch (error) {
         throw error;
      }
   };

   const deleteNews = async (_id: string): Promise<boolean> => {
      try {
         await patchNews(_id);
         const cloneArray = [...news];
         const index = cloneArray.findIndex((row) => row._id === _id);
         console.log(index);
         cloneArray.splice(index, 1);
         setNews([...cloneArray]);
         return true;
      } catch (error) {
         throw error;
      }
   };

   useEffect(() => {
      if (loading) {
         fetchNews();
      }
   }, [loading]);

   return (
      <div>
         {loading ? (
            <h1 className={'Loading'}>Loading...</h1>
         ) : (
            <ul className={'List'}>
               {news.map((row) => {
                  return <News {...row} key={row._id} handleDelete={deleteNews} />;
               })}
            </ul>
         )}
      </div>
   );
};

export default NewsContainer;
