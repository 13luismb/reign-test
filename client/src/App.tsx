import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import NewsContainer from './components/NewsContainer/NewsContainer';

const App = () => {
   return (
      <div className='App'>
         <Header />
         <NewsContainer />
      </div>
   );
};

export default App;
