import axios from "axios";
import { NewsProps as News } from "../interfaces/news.interfaces";

export const getNews = async (): Promise<News[]> => {
   try {
      const news = await axios.get(`${process.env.REACT_APP_SERVER_URL}/news`);
      return news.data;
   } catch (e) {
      throw e;
   }
};

export const patchNews = async (id: string): Promise<string> => {
   try {
      const news = await axios.patch(`${process.env.REACT_APP_SERVER_URL}/news/${id}`);
      return news.data;
   } catch (e) {
      throw e;
   }
};
