# Reign Test

To start the app, just run the command: `docker-compose up -d`

This will start the client on port 3000, and the server on port 5000.

The server should automatically pull the data from the API upon running.
